import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Java5 {
	public static void addWords(String s) {
		DataOutputStream out = null;
		File file = new File("dic.bin");
		try {
			out = new DataOutputStream(new FileOutputStream(file));
			try {
				out.writeUTF(s + "\r\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int compare(String s, String src) {
		int num = 0;
		int npoz = 0;
		while (src.indexOf(s, npoz) != -1) {

			npoz = src.indexOf(s, npoz) + 1;
			num++;
		}
		return num;

	}

	@SuppressWarnings("null")
	public static String FileInput(String src) {
		String s = "";
		File file = new File(src);
		FileReader in = null;
		try {
			in = new FileReader(file);
			char c = ' ';
			int size = (int) file.length();
			for (int i = 0; i < size; i++) {
				try {
					c = (char) in.read();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				s += c;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}

		return s;
	}

	public static String binaryInput() {
		String s = null;
		DataInputStream in = null;

		File file = new File("dic.bin");
		try {

			in = new DataInputStream(new FileInputStream(file));

			s = in.readUTF();

			in.close();
		} catch (IOException ioe) {
			System.out.println("IO problem: " + ioe);
			ioe.printStackTrace();
		}

		return s;
	}

	public static void writeToFile(String s, FileWriter out) {

		try {

			out.write(s + "\r\n");
			out.close();
			out = null;
		} catch (IOException e) {
			System.out.println("������ IO:" + e);
			e.printStackTrace();
			try {
				if (out != null) {

					out.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		FileWriter out = null;
		try {

			out = new FileWriter("out.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String binaryFile = binaryInput();
		String src = FileInput("in.txt");
		String output = "";
		String word = "";
		int npoz = 0;

		while (binaryFile.indexOf("\r", npoz) != -1) {
			
			try {
				word = binaryFile.substring(npoz,
						binaryFile.indexOf("\r", npoz + 1));
			} catch (StringIndexOutOfBoundsException a) {

			}
			output += word.trim() + " ";
			word = word.trim();
			word = word.toLowerCase();
			output += compare(word, src) + "\r\n";
			
			npoz = binaryFile.indexOf("\r", npoz) + 1;
		}

			System.out.println(output);
			writeToFile(output, out);
	}

}
